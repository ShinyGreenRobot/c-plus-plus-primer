# Exercise Solutions Chapter 2

## 2.1 Arithmetic Types
C++ uses the keyword int for the basic integer type. A variable of type int is guaranteed to be of at least 16 bits width, but will in reality often use 32 bits.

If the value that is to be stored is larger than what can be fitted into an int so can the modifiers long and long long be used. Using the long modifier guarantees 32 bits width and long long guarantees 64 bits width.

There is another modifier called short that is guaranteed to have 16 bits width. This modifier is probably not used much anymore but the intention of short is to on certain systems be able to optimize for space if the sufficient memory resources are lacking.

Additional modifiers that can be used with int are signed and unsigned. Signed means that the integer variable can be used to store both negative and positive values. Unsigned means that the variable is limited to storing positive integers, but can on the other hand hold larger values than the signed version. This is because in signed numbers so will one bit be needed to be used up to keep track of if the number is negative or positive.

If floating point values, like for example 3.14159, shall be stored so are one of the floating point types float or double to be used. Double will typically be able to store values with greater precision than float but will also typically use more bits to store the variable. Use the type long double for even higher precision but it can come with costs of both memory consumption and slower program execution.

## 2.13 Defining Variables With Identical Names  

It is possible to have identical names of different variables as long the variables are defined in different scopes.

In the following program fragment so are there two different variables that both are named i, this works because they are defined in different scopes.


```
int i = 42;

int main() {
  int i = 100;
  int j = i;

  /* ... */

}
```

The variable called j will get the value 100 because the variable named i that is defined in the inner scope have precedence over the variable with the same name in the outer global scope.

Note that it is almost always a bad idea to redefine variables in this way since the code becomes confusing. And even if the C++ language itself allows it so will it often break rules of coding standards that are used for projects out in the real world.
