# Exercise Solutions Chapter 3

## 3.1 Using Declarations
Here we have rewritten a couple of programs from previous exercises to utilize using declarations. This technique makes the code less verbose and we might get away with a little less typing if there are multiple occurrences of certain library elements.

The below program is a rewrite of the program from exercise 1.9.

```
#include <iostream>

using std::cout;
using std::endl;

int main() {
  const int kMin = 50;
  const int kMax = 100;
  int sum = 0;
  int i = kMin;

  while (i <= kMax) {
    sum += i;
    ++i;
  }

  cout << "The sum of the numbers from " << kMin << " to " << kMax
       << ", inclusive, is " << sum << endl;

  return 0;
}
```

The below program is a rewrite of the program from exercise 1.10.

```
#include <iostream>

using std::cout;
using std::endl;

int main() {
  int counter = 10;
  while (counter >= 0) {
    cout << counter << endl;
    --counter;
  }
  return 0;
}
```
The below program is a rewrite of the program from exercise 1.11.
```
#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main() {
  cout << "Enter two integers where the first is less or equal to the second: "
      << endl;

  int begin = 0;
  int end = 0;
  cin >> begin;
  cin >> end;

  if (begin > end) {
    cout << "Incorrect input" << endl;
    return 1;
  }

  cout << "The integers in the range from " << begin << " to " << end
      << " are: " << endl;

  int i = begin;
  while (i <= end) {
    cout << i << endl;
    ++i;
  }

  return 0;
}
```
Exercise instructions said to rewrite even more programs from previous exercises but we choose to skip these, at least for now.

## 3.2 Reading Lines and Words
A program that reads the standard input a line at a time. Press ctrl + d or possibly ctrl + z when done, this will trigger printout of how many lines that was entered.

```
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  cout << "Enter some lines of text: " << std::endl;
  string line;
  int num_lines = 0;
  while (getline(cin, line)) {
    ++num_lines;
  }
  cout << endl << num_lines << " lines of text was entered" << endl;
}
```

A program that reads the standard input a word at a time. Press ctrl + d or possibly ctrl + z when done, this will trigger printout of how many words that was entered.

```
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  cout << "Enter some text: " << std::endl;
  string word;
  int num_words = 0;
  while (cin >> word) {
    ++num_words;
  }
  cout << endl << num_words << " words was entered." << endl;
}
```

## 3.3 String Input
We can get input into a string using the function getline or the using the string input operator. The function getline will take everything until the newline character is found or end of file is reached. The string input operator will on the other hand just take all characters until some type of  whitespace character is found.

This means that getline shall be used if entire rows of text like a complete sentence are to be stored in the string. On the other hand so is the string input operator suited for storing a single word in a string.

## 3.4 Comparing Strings
A program that reads two strings and reports if they are equal and if not equal so is it reported what string is the largest.

```
#include <iostream>
#include <string>

int main() {
  std::cout << "Enter two words " << std::endl;

  std::string word_1;
  std::string word_2;
  std::cin >> word_1 >> word_2;

  if (word_1 == word_2) {
    std::cout << "The entered words are equal." << std::endl;
  } else if (word_1 > word_2) {
    std::cout << word_1 << " is larger than " << word_2 << "." << std::endl;
  } else {
    std::cout << word_2 << " is larger than " << word_1 << "." << std::endl;
  }

  return 0;
}
```

A program that reads two strings and reports if they are equally long and if not equally long so is it reported what string is the longest.

```
#include <iostream>
#include <string>

int main() {
  std::cout << "Enter two words " << std::endl;

  std::string word_1;
  std::string word_2;
  std::cin >> word_1 >> word_2;

  std::string::size_type size_1 = word_1.size();
  std::string::size_type size_2 = word_2.size();

  if (size_1 == size_2) {
    std::cout << "The entered words have the same length." << std::endl;
  } else if (size_1 > size_2) {
    std::cout << word_1 << " is longer than " << word_2 << "." << std::endl;
  } else {
    std::cout << word_2 << " is longer than " << word_1 << "." << std::endl;
  }

  return 0;
}
```
