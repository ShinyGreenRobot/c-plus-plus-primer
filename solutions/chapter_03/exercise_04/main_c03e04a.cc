#include <iostream>
#include <string>

int main() {
  std::cout << "Enter two words " << std::endl;

  std::string word_1;
  std::string word_2;
  std::cin >> word_1 >> word_2;

  if (word_1 == word_2) {
    std::cout << "The entered words are equal." << std::endl;
  } else if (word_1 > word_2) {
    std::cout << word_1 << " is larger than " << word_2 << "." << std::endl;
  } else {
    std::cout << word_2 << " is larger than " << word_1 << "." << std::endl;
  }

  return 0;
}
