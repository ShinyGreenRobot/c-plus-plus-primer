#include <iostream>
#include <string>

int main() {
  std::cout << "Enter two words " << std::endl;

  std::string word_1;
  std::string word_2;
  std::cin >> word_1 >> word_2;

  std::string::size_type size_1 = word_1.size();
  std::string::size_type size_2 = word_2.size();

  if (size_1 == size_2) {
    std::cout << "The entered words have the same length." << std::endl;
  } else if (size_1 > size_2) {
    std::cout << word_1 << " is longer than " << word_2 << "." << std::endl;
  } else {
    std::cout << word_2 << " is longer than " << word_1 << "." << std::endl;
  }

  return 0;
}
