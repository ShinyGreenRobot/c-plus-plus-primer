#include <iostream>
#include <string>

int main() {
  std::cout << "Enter some text: " << std::endl;

  std::string word;
  int num_words = 0;

  while (std::cin >> word) {
    ++num_words;
  }

  std::cout << std::endl << num_words << " words was entered." << std::endl;

  return 0;
}
