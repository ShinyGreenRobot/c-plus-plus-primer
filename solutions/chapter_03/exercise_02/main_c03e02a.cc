#include <iostream>
#include <string>

int main() {
  std::cout << "Enter some lines of text: " << std::endl;

  std::string line;
  int num_lines = 0;

  while (std::getline(std::cin, line)) {
    ++num_lines;
  }

  std::cout << std::endl << num_lines << " lines of text was entered."
            << std::endl;

  return 0;
}
