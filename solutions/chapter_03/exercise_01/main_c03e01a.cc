#include <iostream>

using std::cout;
using std::endl;

int main() {
  const int kMin = 50;
  const int kMax = 100;
  int sum = 0;
  int i = kMin;

  while (i <= kMax) {
    sum += i;
    ++i;
  }

  cout << "The sum of the numbers from " << kMin << " to " << kMax
       << ", inclusive, is " << sum << endl;

  return 0;
}
