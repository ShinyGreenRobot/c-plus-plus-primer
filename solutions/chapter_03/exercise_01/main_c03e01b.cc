#include <iostream>

using std::cout;
using std::endl;

int main() {
  int counter = 10;
  while (counter >= 0) {
    cout << counter << endl;
    --counter;
  }
  return 0;
}
