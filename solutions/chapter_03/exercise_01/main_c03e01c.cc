#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main() {
  cout << "Enter two integers where the first is less or equal to the second: "
      << endl;

  int begin = 0;
  int end = 0;
  cin >> begin;
  cin >> end;

  if (begin > end) {
    cout << "Incorrect input" << endl;
    return 1;
  }

  cout << "The integers in the range from " << begin << " to " << end
      << " are: " << endl;

  int i = begin;
  while (i <= end) {
    cout << i << endl;
    ++i;
  }

  return 0;
}
