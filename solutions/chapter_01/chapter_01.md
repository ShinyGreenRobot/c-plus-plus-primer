# Exercise Solutions Chapter 1

## 1.1 Compiler Basics

There are various different C++ compilers, the compiler chosen to be used for the exercises in the book is g++, that is part of the GNU Compiler Collection (GCC). The steps to compile and build a simple program with g++ are explained below.

Assume that we have taken the following program code from the book and stored it in a file called main_c01e01.cc.

```
int main() {
  return 0;
}
```

Under the assumption that we are on a Linux system and have g++ installed so can we build the program by the use of a simple command in a terminal window. Note that for this simple command to work so must we in the terminal be in the same location as where our file main_c01e01.cc is stored. We can check for the existence of the file with the ls command.

```
$ ls
main_c01e01.cc
```

To compile so shall we use the command g++ followed by the name of our source file and then a flag and a name for what we want our program to be called. We choose to call the program main.out.

```
$ g++ main_c01e01.cc -o main.out
```

We will get a new file in our folder called main.out and this file is executable. We use the ls command again to verify the existence of the new file.

```
$ ls
main_c01e01.cc  main.out
```

To run the program so shall we use the name of the program but on Linux will we need to prepend it with ./ when we run it.

```
$ ./main.out
```

Note that this program is not very fun to run since it does not do anything that the user will notice. But we can at least see the value returned after execution have finished. On Linux so is this achieved by the use of an variant of the echo command as follows below.

```
$ echo $?
```

The above command should produce a 0 if run right after execution of our program.

## 1.2 About Return Codes
We will now change the code for the program from exercise 1.1 above and have it return 1 instead of 0. We choose to save this new program in a file called main_c01e02.

```
int main() {
  return 1;
}
```

Return codes other than 0 are often used to indicate to the underlying operating system that the program execution failed in some way. Some systems will act on the error indication, but nothing special happened when we tested on our Linux machine.

```
$ g++ main_c01e02.cc -o main.out#include <iostream>

int main() {
  std::cout << "Enter two different integers:" << std::endl;

  int value_1 = 0;
  int value_2 = 0;
  std::cin >> value_1;
  std::cin >> value_2;

  if (value_1 > value_2) {
    std::cout << "The integers in the range from " << value_1 << " down to "
        << value_2 << ", inclusive, are: " << std::endl;
    int j = value_1;
    while (j >= value_2) {
      std::cout << j << std::endl;
      --j;
    }
  } else {
    std::cout << "The integers in the range from " << value_1 << " up to "
        << value_2 << ", inclusive, are: " << std::endl;
    int i = value_1;
    while (i <= value_2) {
      std::cout << i << std::endl;
      ++i;
    }
  }

  return 0;
}

$ ./main.out
```

The return code value can under Linux be checked in the same way as in Exercise 1.1, but the output should now be 1 since this is what our new program returns.

## 1.3 Hello World!
A program that prints `Hello, World` on the standard output. The endl manipulator is used to have the output on a line of its own.

```
#include <iostream>

int main() {
  std::cout << "Hello, World" << std::endl;
  return 0;
}
```

## 1.4 Number Input and Output
A program that gets two numbers from the user, and then multiplies these.

```
#include <iostream>

int main() {
  std::cout << "Enter two numbers:" << std::endl;

  int factor1 = 0;
  int factor2 = 0;

  std::cin >> factor1 >> factor2;
  std::cout << "The product of " << factor1 << " and " << factor2 << " is "
      << factor1 * factor2 << std::endl;

  return 0;
}
```

## 1.5 Refactoring
Another version of the program from the previous exercise, the large print statement have been separated into several statements. The new version still behaves in the same way as the old version when executed.

```
#include <iostream>

#include <iostream>

int main() {
  std::cout << "Enter two numbers:" << std::endl;

  int factor1 = 0;
  int factor2 = 0;

  std::cin >> factor1 >> factor2;
  std::cout << "The product of " << factor1;
  std::cout << " and " << factor2;
  std::cout << " is " << factor1 * factor2;
  std::cout << std::endl;

  return 0;
}
```

## 1.6 About the Insertion Operator
The following code fragment is not legal and will not compile.

```
std::cout << "The sum of " << v1;
          << " and " << v2;
          << " is " << v1 + v2 << std::endl;
```

The problem is that the insertion operator (<<) is a binary operator, meaning that is must have two operands, and here we only have an operand on both the second and third row. It is also so that the left hand side operand must be of type output stream.

The fragment can be fixed by adding cout.

```
std::cout << "The sum of " << v1;
std::cout << " and " << v2;
std::cout << " is " << v1 + v2 << std::endl;
```

Or alternatively, fix the fragment by chaining of all the insertion operators, since each one returns an output stream, that can be used as operand for the following insertion operator.

```
std::cout << "The sum of " << v1
          << " and " << v2
          << " is " << v1 + v2 << std::endl;
```

## 1.7 Comment Pairs Do Not Nest
It is not possible to nest the `/* */` comment type like, for example is attempted in the below program. Everything after the first `*/` will be considered to be source code.

```
/*
 * comment pairs /* */ cannot nest.
 */
int main() {
  return 0;
}
```

An attempt to compile the program resulted in the following error message.

```
$ error: ‘cannot’ does not name a type  
```
## 1.8 Experiments With C-style Comments
In this exercise so will we solidify our understanding of C-style comments by investigating four different output statements.

First statement:

```
std::cout << "/*";
```

Here we have only the start of a comment pair so the compiler will not treat it as a comment. This statement will compile. The result is that `/*` will be sent to the output stream.

Second statement:
```
std::cout << "*/";
```

As in the first case so is there no complete comment pair. Here we have only the end of a comment pair so the compiler will not treat it as a comment. This statement will compile. The result is that `*/` will be sent to the output stream.

Third statement:

```
std::cout << /* "*/" */;
```

In this third statement so is there a complete comment pair. The compiler will ignore this comment and then try to compile the rest that will correspond to the following statement.

```
std::cout << " */;
```

This is not not valid C++ code so the statement will not compile.

The real intention of the statement is unknown but it could be an attempt to comment out the first statement in this exercise, this can be achieved by the usage of the single line comment.

```
\\ std::cout << "*/";
```

Fourth statement:

```
std::cout << /*  "*/" /* "/*"  */;
```

This fourth statement contains two valid comments that the compiler will ignore and then compile the rest of the code that will correspond to the following statement.

```
std::cout << " /* ";
```

This fourth statement will compile. The result is that the four characters space, slash, star, and space, will be sent to the output stream.

## 1.9 While Statement That Sum Numbers
The following program will calculate the sum of the numbers from 50 to 100, using a while statement to add each term.

```
#include <iostream>

int main() {
  const int kMin = 50;
  const int kMax = 100;
  int sum = 0;
  int i = kMin;

  while (i <= kMax) {
    sum += i;
    i++;
  }

  std::cout << "The sum of the numbers from " << kMin << " to " << kMax
      << ", inclusvie, is " << sum << std::endl;

  return 0;
}
```

## 1.10 Countdown
The following program will print the numbers from 10 down to 0, using a while statement and the decrement operator.

```
#include <iostream>

int main() {
  int counter = 10;
  while (counter >= 0) {
    std::cout << counter << std::endl;
    --counter;
  }
  return 0;
}
```
## 1.11 Print a Range Of Numbers
The following program prints a user defined range of numbers, using a while statement.

```
#include <iostream>

int main() {
  std::cout
      << "Enter two integers where the first is less or equal to the second: "
      << std::endl;

  int begin = 0;
  int end = 0;
  std::cin >> begin;
  std::cin >> end;

  if (begin > end) {
    std::cout << "Incorrect input" << std::endl;
    return 1;
  }

  std::cout << "The integers in the range from " << begin << " to " << end
      << " are: " << std::endl;

  int i = begin;
  while (i <= end) {
    std::cout << i << std::endl;
    ++i;
  }

  return 0;
}
```

## 1.12 For Statement Study
The following for loop will add up all the numbers in the range -100 to 100.

Since every negative number can be paired up with a corresponding positive number so will the sum end up at 0 after the for loop.

```
  int sum = 0;

  for (int i = -100; i <= 100; ++i) {
    sum += i;
  }
```

## 1.13 Rewrite Using For Loops
Three of the programs from above but now with for loops instead of while loops.

```
#include <iostream>

int main() {
  const int kMin = 50;
  const int kMax = 100;
  int sum = 0;

  for (int i = kMin; i <= kMax; ++i) {
    sum += i;
  }

  std::cout << "The sum of the numbers from " << kMin << " to " << kMax
      << ", inclusive, is " << sum << std::endl;

  return 0;
}
```

```
#include <iostream>

int main() {
  for (int counter = 10; counter >= 0; --counter) {
    std::cout << counter << std::endl;
  }
  return 0;
}
```

## 1.14 For Versus While
For and while statements are very similar and all tasks involving repetition can in reality be done with any of the two types.

The for statement is a little more natural to use when the number of times something shall be repeated is known in advance. For statements are also often more concise and easier to read since all the control variables are collected in one place right at the start of the statement.

While statements are natural to use when the control variable is a boolean flag.

## 1.15 Error Messages
Here we intentionally try to compile some corrupt programs to familiarize ourself with compiler error messages. Each program is followed by a shortened version of the error messages received when compiling.

Missing semicolon after return 0.

```
int main() {
  return 0
}
```

```
$ error: expected ‘;’ before ‘}’ token
```

Missing parenthesis after main.

```
int main( {
  return 0;
}
```

```
$ error: cannot declare ‘::main’ to be a global variable
$ error: expected primary-expression before ‘return’
$ error: expected ‘}’ before ‘return’
$ error:expected ‘)’ before ‘return’
$ error: expected declaration before ‘}’ token

```

Ending line with colon instead of semicolon.

```
#include <iostream>

int main() {
  std::cout << "Hello, world" << std::endl:
  return 0;
}
```
```
error: expected ‘;’ before ‘:’ token
```

Missing quotes around string literal.

```
#include <iostream>

int main() {
  std::cout << Hello, world << std::endl;
  return 0;
}
```

```
error: ‘Hello’ was not declared in this scope
note: suggested alternative: ‘ftello’
error: ‘world’ was not declared in this scope
```

Missing output operator.

```
#include <iostream>

int main() {
  std::cout << "Hello, world" std::endl;
  return 0;
}
```

```
error: expected ‘;’ before ‘std’
```

## 1.16 Reading Multiple Values
A program that calculates and prints the sum of a set of integers entered by the user. End of entering integers is achieved by control + z on Windows machines and often by control + d on other types of machines.

#include <iostream>

```
int main() {
  std::cout << "Enter a set of numbers to be summed:" << std::endl;

  int next_value_to_sum = 0;
  int sum = 0;

  while (std::cin >> next_value_to_sum) {
    sum += next_value_to_sum;
  }

  std::cout << "The sum of the numbers is " << sum << std::endl;

  return 0;
}
```

## 1.17 Predict the Output
The objective is to study the below program, and predict what happens if all input numbers are equal. Then also predict the outcome if all numbers are different.

```
#include <iostream>

int main() {

  int current_value = 0;  // the number we are currently counting;
  int new_value = 0;     // newly read value

  // read first number and ensure that we have data to process
  if (std::cin >> current_value) {
    int cnt = 1;  // store the count for the current value we're processing

    while (std::cin >> new_value) {  // read the remaining numbers
      if (new_value == current_value)  // if the values are the same
        ++cnt;  // add 1 to cnt
      else {  // otherwise, print the count for the previous value
        std::cout << current_value << " occurs " << cnt << " times" << std::endl;
        current_value = new_value;  // remember the new value
        cnt = 1;  // reset the counter
      }
    }  // while loop ends here

    // remember to print the count for the last value in the file
    std::cout << current_value << " occurs " << cnt << " times" << std::endl;

  }  // outermost if statement ends here

  return 0;
}

```

Output is triggered in two ways firstly when `new_value` is different from `current_value` and secondly when there is no more input, caused by end of file. (Remember that end of file can be faked with ctrl + z or ctrl + d if entering numbers by hand.) This means that if all input numbers are equal so will there be will be no output from the program until we reach the end of file. The number count is then finally printed out once and the count will be equal to how many input numbers there were.

On the other hand if all numbers are different so will there be a printout from the program on each input value, but the very first. This is because the else part of the program will be chosen every time we reach this part of the code. Finally when there is no more input so will the count for very last input number be printed. The count will be 1 for all numbers.

## 1.18 Test the Prediction
We shall now compile and test run the program from the previous exercise.

We have prepared two files with a little data that we will feed the program.

The file equal.txt holds the following data.

```
1 1 1 1 1
```

And the file different.txt holds the following data.

```
1 2 3 4 5
```

Our program is called a.out and test runs on the Linux command line gave the following results.

```
$ ./a.out < equal.txt
1 occurs 5 times
$ ./a.out < different.txt
1 occurs 1 times
2 occurs 1 times
3 occurs 1 times
4 occurs 1 times
5 occurs 1 times
```
## 1.19 Exercise 11 Revisited
A program that prints numbers in a range choses by the user, much like the program in exercise 1.11 but this new version will also handle the case where the first chosen number is larger than the second.

```
#include <iostream>

int main() {
  std::cout << "Enter two different integers:" << std::endl;

  int value_1 = 0;
  int value_2 = 0;
  std::cin >> value_1;
  std::cin >> value_2;

  if (value_1 > value_2) {
    std::cout << "The integers in the range from " << value_1 << " down to "
        << value_2 << ", inclusive, are: " << std::endl;
    int j = value_1;
    while (j >= value_2) {
      std::cout << j << std::endl;
      --j;
    }
  } else {
    std::cout << "The integers in the range from " << value_1 << " up to "
        << value_2 << ", inclusive, are: " << std::endl;
    int i = value_1;
    while (i <= value_2) {
      std::cout << i << std::endl;
      ++i;
    }
  }

  return 0;
}
```
## 1.20 First use of sales_item.h
A program that prints that reads a set of book sales items and prints them to the standard output. The program uses code provided by the authors of the book in the form of a header file that we include at the start of our program code. Note that the originally supplied file is called Sales_item.h but we have chosen to rename it sales_item.h, since many coding style conventions have the rule that all letters in files shall be lower case.

Here is the program.

```
#include "sales_item.h"

#include <iostream>

int main() {
  Sales_item sales_item;

  while (std::cin >> sales_item) {
    std::cout << sales_item << std::endl;
  }

  return 0;
}
```

We use a file called book_sales.txt with sales data to test the program since it is tedious and error prone to enter data by hand. This file comes with the files provided by the authors of the book and a copy is also included in a sub folder to this document.

A sample run of the program can be seen below.

```
$ ./main.out < book_sales.txt
0-201-70353-X 4 99.96 24.99
0-201-82470-1 4 181.56 45.39
0-201-88954-4 2 30 15
0-201-88954-4 5 60 12
0-201-88954-4 7 84 12
0-201-88954-4 2 24 12
0-399-82477-1 2 90.78 45.39
0-399-82477-1 3 136.17 45.39
0-201-78345-X 3 60 20
0-201-78345-X 2 50 25
```

## 1.21 Summing Two Book Sales
A program that reads two book sales items and presents the sum. We can simply use the ordinary + operator to sum if we include the sales item header file because the content of this file provides functionality for adding sales items.

```
#include "sales_item.h"

#include <iostream>

int main() {
  Sales_item sales_item_1;
  Sales_item sales_item_2;

  std::cin >> sales_item_1 >> sales_item_2;

  std::cout << "   " << sales_item_1 << std::endl;
  std::cout << "   " << sales_item_2 << std::endl;
  std::cout << " + ---------------------" << std::endl;
  std::cout << "   " << sales_item_1 + sales_item_2 << std::endl;

  return 0;
}
```

Note that the two sales items objects feed into to the program must have the same ISBN due to that sales item + operator used requires this. The file book_sales_c01e21.txt located in the sub folder dedicated for files for this exercise holds two sales items with the same ISBN, this file can be used to test the program.

```
$ ./main.out < book_sales_c01e21.txt
   0-201-88954-4 2 30 15
   0-201-88954-4 5 60 12
 + ---------------------
  0-201-88954-4 7 90 12.8571
```
