#include <iostream>

int main() {

  int current_value = 0;  // the number we are currently counting;
  int new_value = 0;     // newly read value

  // read first number and ensure that we have data to process
  if (std::cin >> current_value) {
    int cnt = 1;  // store the count for the current value we're processing

    while (std::cin >> new_value) {  // read the remaining numbers
      if (new_value == current_value)  // if the values are the same
        ++cnt;  // add 1 to cnt
      else {  // otherwise, print the count for the previous value
        std::cout << current_value << " occurs " << cnt << " times" << std::endl;
        current_value = new_value;  // remember the new value
        cnt = 1;  // reset the counter
      }
    }  // while loop ends here

    // remember to print the count for the last value in the file
    std::cout << current_value << " occurs " << cnt << " times" << std::endl;

  }  // outermost if statement ends here

  return 0;
}

