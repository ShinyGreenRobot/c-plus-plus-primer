#include <iostream>

int main() {
  std::cout << "Enter a set of numbers to be summed:" << std::endl;

  int next_value_to_sum = 0;
  int sum = 0;

  while (std::cin >> next_value_to_sum) {
    sum += next_value_to_sum;
  }

  std::cout << "The sum of the numbers is " << sum << std::endl;

  return 0;
}
