#include <iostream>

int main() {
  std::cout << "Enter two numbers:" << std::endl;

  int factor1 = 0;
  int factor2 = 0;

  std::cin >> factor1 >> factor2;
  std::cout << "The product of " << factor1 << " and " << factor2 << " is "
      << factor1 * factor2 << std::endl;
  
  return 0;
}
