#include <iostream>

int main() {
  std::cout << "Enter two different integers:" << std::endl;

  int value_1 = 0;
  int value_2 = 0;
  std::cin >> value_1;
  std::cin >> value_2;

  if (value_1 > value_2) {
    std::cout << "The integers in the range from " << value_1 << " down to "
        << value_2 << ", inclusive, are: " << std::endl;
    int j = value_1;
    while (j >= value_2) {
      std::cout << j << std::endl;
      --j;
    }
  } else {
    std::cout << "The integers in the range from " << value_1 << " up to "
        << value_2 << ", inclusive, are: " << std::endl;
    int i = value_1;
    while (i <= value_2) {
      std::cout << i << std::endl;
      ++i;
    }
  }

  return 0;
}
