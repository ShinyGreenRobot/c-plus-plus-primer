#include <iostream>

int main() {
  std::cout
      << "Enter two integers where the first is less or equal to the second: "
      << std::endl;

  int begin = 0;
  int end = 0;
  std::cin >> begin;
  std::cin >> end;

  if (begin > end) {
    std::cout << "Incorrect input" << std::endl;
    return 1;
  }

  std::cout << "The integers in the range from " << begin << " to " << end
      << " are: " << std::endl;

  int i = begin;
  while (i <= end) {
    std::cout << i << std::endl;
    ++i;
  }

  return 0;
}
