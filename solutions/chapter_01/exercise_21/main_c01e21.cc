#include "sales_item.h"

#include <iostream>

int main() {
  Sales_item sales_item_1;
  Sales_item sales_item_2;

  std::cin >> sales_item_1 >> sales_item_2;

  std::cout << "   " << sales_item_1 << std::endl;
  std::cout << "   " << sales_item_2 << std::endl;
  std::cout << " + ---------------------" << std::endl;
  std::cout << "   " << sales_item_1 + sales_item_2 << std::endl;

  return 0;
}
