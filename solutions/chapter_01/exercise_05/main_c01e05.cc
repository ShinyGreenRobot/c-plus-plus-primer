#include <iostream>

int main() {
  std::cout << "Enter two numbers:" << std::endl;

  int factor1 = 0;
  int factor2 = 0;

  std::cin >> factor1 >> factor2;
  std::cout << "The product of " << factor1;
  std::cout << " and " << factor2;
  std::cout << " is " << factor1 * factor2;
  std::cout << std::endl;
  
  return 0;
}