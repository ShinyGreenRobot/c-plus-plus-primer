#include <iostream>

int main() {
  const int kMin = 50;
  const int kMax = 100;
  int sum = 0;

  for (int i = kMin; i <= kMax; ++i) {
    sum += i;
  }

  std::cout << "The sum of the numbers from " << kMin << " to " << kMax
      << ", inclusive, is " << sum << std::endl;

  return 0;
}
