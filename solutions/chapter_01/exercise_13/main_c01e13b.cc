#include <iostream>

int main() {
  for (int counter = 10; counter >= 0; --counter) {
    std::cout << counter << std::endl;
  }
  return 0;
}
